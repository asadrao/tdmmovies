package com.example.movieapp.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.model.SearchDBResponse
import com.example.movieapp.mysubapplication.MySubApplication
import com.example.movieapp.rvAdapter.MoviesListAdapter
import com.example.movieapp.rvAdapter.SearchMovieAdapter
import com.example.movieapp.utils.Utils
import com.example.movieapp.viewmodel.MovieViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.toolbar.view.*


class SearchMovie : Fragment() {

    var utils: Utils? = MySubApplication.component!!.provideUtils()
    var viewModel: MovieViewModel? = null
    var progressBar: ProgressBar? = null
    var mShimmerViewContainer: ShimmerFrameLayout? = null
    lateinit var adapter: SearchMovieAdapter
    var rootView: View? = null

    var movieResultsList: List<SearchDBResponse> = ArrayList()

    lateinit var adView: AdView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
       // return inflater.inflate(R.layout.fragment_search_movies, container, false)

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_search_movies, container, false)

            MobileAds.initialize(context, "ca-app-pub-3940256099942544~3347511713")
            adView = rootView?.findViewById(R.id.adView)!!
            val adRequest = com.google.android.gms.ads.AdRequest.Builder().build()
            adView.loadAd(adRequest)

            toolBarViewsCustomization(rootView)
            setUpRecyclerViewWithAdapter(rootView)
            //fetchMovieList()
        }
        return rootView
    }

    private fun setUpRecyclerViewWithAdapter(view: View?) {

        progressBar = view?.findViewById(R.id.progress_bar)
        var recyclerView: RecyclerView = view?.findViewById(R.id.rv) as RecyclerView
        //mShimmerViewContainer = view?.findViewById(R.id.shimmer_view_container)
        var gridLayoutManager: GridLayoutManager = GridLayoutManager(context, 2)
        recyclerView!!.layoutManager = gridLayoutManager
        adapter = SearchMovieAdapter()
        recyclerView.adapter = adapter

    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun getMoviesList(query:String) {
        viewModel = ViewModelProviders.of(this).get<MovieViewModel>(MovieViewModel::class.java)
        viewModel?.moviesQuery(query)?.observe(
            this,
            Observer<List<SearchDBResponse?>> { movieList ->



                for(s in movieList){

                    if (s?.getTitle()?.contains(query.toLowerCase())!!){
                        //movieResultsList.addAll(searchDBResponses);
                        adapter.setQueryList(movieList);
                        Log.e("SSS",movieList.size.toString())

                    }
                }

            })
    }
    private fun toolBarViewsCustomization(view: View?) {

        val searchView : SearchView? = view?.findViewById(R.id.iv_search_icon)
        searchView?.visibility=View.VISIBLE


        searchView?.setMaxWidth(Integer.MAX_VALUE);

        searchView!!.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                Log.e("myQuery",query)
                getMoviesList(query)
                searchView!!.clearFocus()
                return false
            }

            override fun onQueryTextChange(query: String): Boolean {
                // filter recycler view when text is changed
                return false
            }
        });        view?.tv_search?.visibility = View.GONE
        view?.iv_home?.visibility = View.VISIBLE
        view?.iv_home?.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_searchMovie_to_moviesList2)
        }

    }
}