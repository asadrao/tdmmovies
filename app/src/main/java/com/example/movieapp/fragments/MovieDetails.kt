package com.example.movieapp.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.example.movieapp.R
import com.example.movieapp.constantVal.ConstantVal
import com.example.movieapp.databinding.ActivityMovieDetailBinding
import com.example.movieapp.databinding.FragmentMovieDetailsBinding
import com.example.movieapp.databinding.MoviesListBinding
import com.example.movieapp.model.detailmodel.MoviesDetailResult
import com.example.movieapp.mysubapplication.MySubApplication
import com.example.movieapp.utils.Utils
import com.example.movieapp.viewmodel.MovieViewModel
import com.example.movieapp.views.PlayerActivity
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.android.synthetic.main.toolbar.view.*

class MovieDetails : Fragment() {

    var utils: Utils? = MySubApplication.component!!.provideUtils()
    var mShimmerViewContainer: ShimmerFrameLayout? = null
    var viewModel2: MovieViewModel? = null
    lateinit var movieDetailBinding: FragmentMovieDetailsBinding
    var progressBar: ProgressBar? = null
    var movieId = 0
    lateinit var adView: AdView
    lateinit var playVideo: ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //return inflater.inflate(R.layout.fragment_movie_details, container, false)

        movieDetailBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_movie_details, container, false)
        var view: View = movieDetailBinding.root


        MobileAds.initialize(context, "ca-app-pub-3940256099942544~3347511713")
        adView = view?.findViewById(R.id.adView)!!
        val adRequest = com.google.android.gms.ads.AdRequest.Builder().build()
        adView.loadAd(adRequest)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        toolBarViewsCustomization(view)
        getMovieId()
        fetchMovieDetails()
    }

    private fun initViews(view: View) {
        // movieDetailBinding = DataBindingUtil.setContentView(this@MovieDetailActivity, R.layout.activity_movie_detail)
        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container)
        progressBar = view.findViewById(R.id.progress_bar)
        playVideo = view?.findViewById(R.id.play_video) as ImageView
        playVideo.setOnClickListener {
            val intent = Intent(context, PlayerActivity::class.java)
            intent.putExtra("MEDIA_URI", "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerEscapes.mp4")
            startActivity(intent)
        }
    }

    private fun toolBarViewsCustomization(view: View?) {
        view?.tv_search?.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_movieDetails_to_searchMovie)
            //iv_home.visibility=View.INVISIBLE
        }
    }

    private fun getMovieId() {

        movieId = arguments?.getInt(ConstantVal.MOVIE_ID)!!
        Log.e("name: ", movieId.toString() + "")


    }

    @SuppressLint("FragmentLiveDataObserve")
    private fun getMoviesDetail(movId: Int) {
        viewModel2 = ViewModelProviders.of(this).get<MovieViewModel>(MovieViewModel::class.java)
        //viewModel2?.moviesDetails(movId)?.removeObservers(this)
        viewModel2?.moviesDetails(movId)?.observe(
            this,
            Observer<MoviesDetailResult?> { movieList ->
                movieDetailBinding?.movieDetails = movieList
                utils?.logMessage("MY LS", movieList?.getTitle())
                utils!!.shimmerProgressViewsUnVisible(mShimmerViewContainer, progressBar)
            })
    }

    private fun fetchMovieDetails() {
        if (utils?.isConnectingToInternet(context)!!) {
            getMoviesDetail(movieId);
        } else {
            Handler().postDelayed({
                container_detail.visibility = View.GONE
                utils?.shimmerProgressViewsUnVisible(mShimmerViewContainer, progressBar)
                utils?.toastMessage(context, "Internet Time Out")
            }, ConstantVal.SHIMMER_TIME_DELAY)
        }
    }

    //mShimmerViewContainer Start Animation
    override fun onResume() {
        super.onResume()
        mShimmerViewContainer?.startShimmer()
    }

    //mShimmerViewContainer stop Animation
    override fun onPause() {
        mShimmerViewContainer?.stopShimmer()
        super.onPause()
    }



}