package com.example.movieapp.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.constantVal.ConstantVal
import com.example.movieapp.model.MovieResult
import com.example.movieapp.mysubapplication.MySubApplication
import com.example.movieapp.rvAdapter.MoviesListAdapter
import com.example.movieapp.utils.Utils
import com.example.movieapp.viewmodel.MovieViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.toolbar.view.*


class MoviesList : Fragment() {

    var utils: Utils? = MySubApplication.component!!.provideUtils()
    var viewModel: MovieViewModel? = null
    var progressBar: ProgressBar? = null
    var mShimmerViewContainer: ShimmerFrameLayout? = null
    lateinit var adapter: MoviesListAdapter
    var rootView: View? = null

    lateinit var adView: AdView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_movies_list, container, false)

            MobileAds.initialize(context, "ca-app-pub-3940256099942544~3347511713")
            adView = rootView?.findViewById(R.id.adView)!!
            val adRequest = com.google.android.gms.ads.AdRequest.Builder().build()
            adView.loadAd(adRequest)


            toolBarViewsCustomization(rootView)
            setUpRecyclerViewWithAdapter(rootView)
            fetchMovieList()
        }
        return rootView
        //return inflater.inflate(R.layout.fragment_movies_list, container, false)
    }


    // setUp RecyclerView With Movies Adapter
    private fun setUpRecyclerViewWithAdapter(view: View?) {

        progressBar = view?.findViewById(R.id.progress_bar)
        var recyclerView: RecyclerView = view?.findViewById(R.id.rv) as RecyclerView
        mShimmerViewContainer = view?.findViewById(R.id.shimmer_view_container)
        var gridLayoutManager: GridLayoutManager = GridLayoutManager(context, 2)
        recyclerView!!.layoutManager = gridLayoutManager
        adapter = MoviesListAdapter(context)
        recyclerView.adapter = adapter


    }

    private fun toolBarViewsCustomization(view:View?) {
        view?.tv_search?.setOnClickListener {
           // Navigation.findNavController(it).navigate(R.id.action_moviesList_to_searchMovie)
            //iv_home.visibility=View.INVISIBLE
        }
    }

    // fetch all movie list getMovieList function in this function
    private fun fetchMovieList() {
        if (utils?.isConnectingToInternet(context)!!) {
            getMoviesList();
        } else {
            Handler().postDelayed({
                utils?.shimmerProgressViewsUnVisible(mShimmerViewContainer, progressBar)
                utils?.toastMessage(context, "Internet Time Out")
            }, ConstantVal.SHIMMER_TIME_DELAY)
        }
    }

    // Get all Movies List using observer and viewModel initialize in this function
    @SuppressLint("FragmentLiveDataObserve")
    private fun getMoviesList() {
        viewModel = ViewModelProviders.of(this).get<MovieViewModel>(MovieViewModel::class.java)
        viewModel?.moviesList()?.observe(
            this,
            Observer<List<MovieResult?>> { movieList ->
                adapter?.setMoviesList(movieList)
                utils!!.shimmerProgressViewsUnVisible(mShimmerViewContainer, progressBar)
            })
    }

    override fun onResume() {
        super.onResume()
        mShimmerViewContainer?.startShimmer()
    }

    //mShimmerViewContainer stop Animation
    override fun onPause() {
        mShimmerViewContainer?.stopShimmer()
        super.onPause()
    }
}