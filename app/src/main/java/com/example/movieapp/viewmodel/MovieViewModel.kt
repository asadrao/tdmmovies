package com.example.movieapp.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.movieapp.model.MovieResult
import com.example.movieapp.model.SearchDBResponse
import com.example.movieapp.model.detailmodel.MoviesDetailResult
import com.example.movieapp.movierepository.Repository
import com.example.movieapp.mysubapplication.MySubApplication

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    //var repository: Repository = MySubApplication.component!!.provideRepository()!!
    var repository: Repository?=null
    init {
          repository = Repository()
     }
    // Get Live data of Movies List
     fun moviesList(): LiveData<List<MovieResult>> {
        Log.e("LiveData","moviesList ViewModel Access")
        return repository!!.getAllMoviesList()
    }
    fun moviesDetails(movieId:Int): LiveData<MoviesDetailResult> {
        Log.e("LiveData","moviesDetails ViewModel Access")
        return repository!!.getAllMoviesDetail(movieId)
    }

    fun moviesQuery(query:String): MutableLiveData<List<SearchDBResponse>> {
        Log.e("LiveData","moviesDetails ViewModel Access")
        return repository!!.getSearchList(query)
    }
}
