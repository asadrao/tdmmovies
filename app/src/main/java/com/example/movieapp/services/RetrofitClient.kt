package com.example.movieapp.services

import com.example.movieapp.constantVal.ConstantVal
import com.example.movieapp.interfaces.MovieDataServices
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

  object RetrofitClient {


     val getClient: MovieDataServices
         get() {

             val gson = GsonBuilder()
                 .setLenient()
                 .create()
             val interceptor = HttpLoggingInterceptor()
             interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
             val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

             val retrofit = Retrofit.Builder()
                 .baseUrl(ConstantVal.BASE_URL)
                 .client(client)
                 .addConverterFactory(GsonConverterFactory.create(gson))
                 .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                 .build()

             return retrofit.create(MovieDataServices::class.java)

         }




 }