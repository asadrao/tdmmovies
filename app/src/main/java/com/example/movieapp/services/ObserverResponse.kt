package com.example.movieapp.services

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.movieapp.model.MovieDbResponse
import com.example.movieapp.model.MovieResult
import com.example.movieapp.model.SearchDBResponse
import com.example.movieapp.model.SearchResult
import com.example.movieapp.model.detailmodel.MoviesDetailResult
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import javax.inject.Inject


class ObserverResponse @Inject constructor(val context: Context) {

    // observer of Movies List
    fun getAllMoviesListObserver(liveDataMoviesList: MutableLiveData<List<MovieResult>>): Observer<MovieDbResponse?>? {

        return object : Observer<MovieDbResponse?> {
            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }

            override fun onNext(t: MovieDbResponse) {

                liveDataMoviesList.value = t.getResults() as List<MovieResult>?
                Log.e("data2", liveDataMoviesList.toString())
            }

            override fun onError(e: Throwable) {
            }

        }
    }

    fun getALlMoviesDetailObserver(movieDetailResult: MutableLiveData<MoviesDetailResult>): Observer<MoviesDetailResult?>? {


        return object : Observer<MoviesDetailResult?>{
            override fun onComplete() {
                Log.e("onComplete", "onComplete")

            }

            override fun onSubscribe(d: Disposable) {
                Log.e("onSubscribe", "onSubscribe")

            }

            override fun onNext(t: MoviesDetailResult) {
                movieDetailResult.value = t
                Log.e("data", t.toString())
            }

            override fun onError(e: Throwable) {
                Log.e("onError", "onError"+e.message)

            }

        }
    }

    fun getMoviesQueryResult(searchListMutableLiveData: MutableLiveData<List<SearchDBResponse>>): Observer<SearchResult?>? {
        return object : Observer<SearchResult?> {
            override fun onSubscribe(d: Disposable) {}


            override fun onError(e: Throwable) {}
            override fun onComplete() {}
            override fun onNext(t: SearchResult) {
                if (t != null) {
                    searchListMutableLiveData.setValue(t.getResults() as List<SearchDBResponse>)
                }            }
        }
    }

}