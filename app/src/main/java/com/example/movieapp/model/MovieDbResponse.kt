package com.example.movieapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MovieDbResponse {

    @SerializedName("page")
    @Expose
    private val page: Int? = null
    @SerializedName("total_results")
    @Expose
    private val totalResults: Int? = null
    @SerializedName("total_pages")
    @Expose
    private val totalPages: Int? = null
    @SerializedName("results")
    @Expose
    private val results: List<MovieResult>? = null
    fun getResults(): List<MovieResult?>? {
        return results
    }


}