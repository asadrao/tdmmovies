package com.example.movieapp.interfaces

import com.example.movieapp.model.MovieDbResponse
import com.example.movieapp.model.SearchResult
import com.example.movieapp.model.detailmodel.MoviesDetailResult
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieDataServices {


    //@GET("movie/popular")
    //open fun getAllMovies(@Query("api_key") apiKey: String?, @Query("language") language: String?, @Query("page") page: Int): Call<MovieDbResponse>

    @GET("movie/popular")
    open fun getAllMoviesObserv(
        @Query("api_key") apiKey: String?,
        @Query("language") language: String?,
        @Query("page") page: Int
    ): Observable<MovieDbResponse>

    @GET("movie/{id}")
    open fun getDetails(
        @Path("id") id: Int?,
        @Query("api_key") apiKey: String
    ): Observable<MoviesDetailResult>

    @GET("search/movie")
    fun searchMovies(
        @Query("api_key") apiKey: String?,
        @Query("query") query: String
    ): Observable<SearchResult>
}