package com.example.movieapp.interfaces

import android.view.View
import com.example.movieapp.model.MovieResult


interface CustomClickListener {

    fun cardClicked(result: MovieResult?, view: View?)

}