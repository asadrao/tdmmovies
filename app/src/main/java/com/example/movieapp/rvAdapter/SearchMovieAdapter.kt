package com.example.movieapp.rvAdapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.databinding.MoviesListBinding
import com.example.movieapp.databinding.SearchMovieListBinding
import com.example.movieapp.model.MovieResult
import com.example.movieapp.model.SearchDBResponse
import java.util.*


class SearchMovieAdapter : RecyclerView.Adapter<SearchMovieAdapter.SearchViewHolder>() {


    private var mList: List<SearchDBResponse> = ArrayList()



    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchMovieAdapter.SearchViewHolder {
        //return SearchViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.search_movie_list, parent, false))
        val searchListBinding: SearchMovieListBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.search_movie_list, parent, false)
        return SearchMovieAdapter.SearchViewHolder(searchListBinding)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: SearchMovieAdapter.SearchViewHolder, position: Int) {

        val result: SearchDBResponse = mList.get(position)
        Log.e("result",result.getId().toString())
        holder.listBind.searchDbResponse = result


    }

    fun setQueryList(list: List<SearchDBResponse?>?) {

        this.mList = list as List<SearchDBResponse>
        Log.e("myList",mList.size.toString())
        notifyDataSetChanged()
    }

    class SearchViewHolder(itemView: SearchMovieListBinding) : RecyclerView.ViewHolder(itemView.root) {
        var listBind: SearchMovieListBinding = itemView

    }
}