package com.example.movieapp.rvAdapter

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.movieapp.R
import com.example.movieapp.constantVal.ConstantVal
import com.example.movieapp.databinding.MoviesListBinding
import com.example.movieapp.fragments.MovieDetails
import com.example.movieapp.fragments.MoviesList
import com.example.movieapp.fragments.MoviesListDirections
import com.example.movieapp.interfaces.CustomClickListener
import com.example.movieapp.model.MovieResult
import com.example.movieapp.views.MainActivity
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import java.util.*


class MoviesListAdapter(context: Context?) :
    RecyclerView.Adapter<MoviesListAdapter.MoviesListViewHolder>(), CustomClickListener {

    private var mList: List<MovieResult> = ArrayList()
    var ctx: Context = context!!
    private lateinit var mInterstitialAd: InterstitialAd



    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MoviesListAdapter.MoviesListViewHolder {
        val listBinding: MoviesListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.movies_list,
            parent,
            false
        )
        return MoviesListViewHolder(listBinding)
    }

    override fun getItemCount(): Int {
        Log.e("mList Size", "" + mList.size)
        return mList.size
    }

    override fun onBindViewHolder(holder: MoviesListAdapter.MoviesListViewHolder, position: Int) {

        mInterstitialAd = InterstitialAd(ctx)
        mInterstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"
        mInterstitialAd.loadAd(AdRequest.Builder().build())

        val result: MovieResult = mList.get(position)
        Log.e("result", result.getId().toString())
        holder.listBind.movieList = result
        holder.listBind.setItemClickListener(this);

    }

    fun setMoviesList(list: List<MovieResult?>?) {
        this.mList = list as List<MovieResult>
        Log.e("myList2", mList.size.toString())
        notifyDataSetChanged()
    }

    class MoviesListViewHolder(itemView: MoviesListBinding) :
        RecyclerView.ViewHolder(itemView.root) {

        var listBind: MoviesListBinding = itemView
    }

    override fun cardClicked(result: MovieResult?, view: View?) {

        // send id  for activity
//        val id = result!!.getId()!!
//        val intent = Intent(ctx, MovieDetailActivity::class.java)
//        intent.putExtra(ConstantVal.MOVIE_ID, id)
//        ctx.startActivity(intent)


        if (mInterstitialAd.isLoaded) {
            mInterstitialAd.show()
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.")
        }
        val bundle = Bundle()
        val id = result!!.getId()!!
        bundle.putInt(ConstantVal.MOVIE_ID, id)
        Navigation.findNavController(view!!)
            .navigate(R.id.action_moviesList_to_movieDetails, bundle)
        //Navigation.findNavController(view!!).navigate(nextAction,bundle)


        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                // Code to be executed when an ad request fails.
            }

            override fun onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            override fun onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            override fun onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            override fun onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                mInterstitialAd.loadAd(AdRequest.Builder().build())

            }
        }


        //Log.e("ID: ", id.toString() + "")
    }
}