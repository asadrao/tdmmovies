package com.example.movieapp.constantVal

class ConstantVal {



    companion object{
        const val TIME_DELAY = 2000 as Long
        const val SHIMMER_TIME_DELAY = 4000 as Long
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val API_KEY = "0865a249a44d235921fc79bcb4d6e358"
        const val IMAGE_BASE_URL = "http://image.tmdb.org/t/p/w500"
        const val LANGUAGE = "en-US"
        const val MOVIE_ID = "id"
    }

}