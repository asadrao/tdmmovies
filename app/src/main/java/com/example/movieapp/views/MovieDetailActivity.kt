package com.example.movieapp.views

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.movieapp.R
import com.example.movieapp.constantVal.ConstantVal
import com.example.movieapp.databinding.ActivityMovieDetailBinding
import com.example.movieapp.model.detailmodel.Genre
import com.example.movieapp.model.detailmodel.MoviesDetailResult
import com.example.movieapp.mysubapplication.MySubApplication
import com.example.movieapp.utils.Utils
import com.example.movieapp.viewmodel.MovieViewModel
import com.facebook.shimmer.ShimmerFrameLayout
import kotlinx.android.synthetic.main.activity_movie_detail.*

class MovieDetailActivity : AppCompatActivity() {
    var utils: Utils? = MySubApplication.component!!.provideUtils()
    var mShimmerViewContainer: ShimmerFrameLayout? = null
    var viewModel2: MovieViewModel? = null
    var movieDetailBinding: ActivityMovieDetailBinding? = null
    var progressBar: ProgressBar? = null
    var movieId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initViews()
        getMovieId()
        fetchMovieDetails()


    }

    private fun getMoviesDetail(movId: Int) {
        viewModel2 = ViewModelProviders.of(this).get<MovieViewModel>(MovieViewModel::class.java)
        //viewModel2?.moviesDetails(movId)?.removeObservers(this)
        viewModel2?.moviesDetails(movId)?.observe(
            this,
            Observer<MoviesDetailResult?> { movieList ->
                movieDetailBinding?.movieDetails = movieList
                utils!!.shimmerProgressViewsUnVisible(mShimmerViewContainer, progressBar)
            })
    }

    private fun fetchMovieDetails() {
        if (utils?.isConnectingToInternet(this)!!) {
            getMoviesDetail(movieId);
        } else {
            Handler().postDelayed({
                container_detail.visibility= View.GONE
                utils?.shimmerProgressViewsUnVisible(mShimmerViewContainer, progressBar)
                utils?.toastMessage(this, "Internet Time Out")
            }, ConstantVal.SHIMMER_TIME_DELAY)
        }
    }

    private fun initViews() {
        movieDetailBinding =
            DataBindingUtil.setContentView(this@MovieDetailActivity, R.layout.activity_movie_detail)
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container)
        progressBar = findViewById(R.id.progress_bar)
    }

    private fun getMovieId() {

        val intent = getIntent();
        movieId = intent.getIntExtra(ConstantVal.MOVIE_ID, -1)


    }

    //mShimmerViewContainer Start Animation
    override fun onResume() {
        super.onResume()
        mShimmerViewContainer?.startShimmer()
    }

    //mShimmerViewContainer stop Animation
    override fun onPause() {
        mShimmerViewContainer?.stopShimmer()
        super.onPause()
    }


}
