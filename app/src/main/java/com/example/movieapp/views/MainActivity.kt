package com.example.movieapp.views

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import com.example.movieapp.R


class MainActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //setToolbar()
    }

    private fun setToolbar() {

        toolbar = findViewById(R.id.toolbar) as Toolbar
        toolbar.setTitleTextColor(Color.WHITE)
        toolbar.title = "n.toString()"
        setSupportActionBar(toolbar)

//        tv_search.setOnClickListener {
//            Navigation.findNavController(it).navigate(R.id.action_moviesList_to_searchMovie)
//        }

    }


//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.menu_main, menu)
//        return true
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//
//        //val navigated = NavigationUI.onNavDestinationSelected(item!!, navController)
//        //return navigated || super.onOptionsItemSelected(item)
//
//        val id = item?.itemId
//        val navController = Navigation.findNavController(this, R.id.fragment)
//        NavigationUI.setupActionBarWithNavController(this, navController)
//
//        Log.e("ASAd", "ASAD")
//
//        return true
//
//
//    }
}
