package com.example.movieapp.mysubapplication

import android.app.Application
import android.content.Context
import com.danikula.videocache.HttpProxyCacheServer
import com.example.movieapp.dagger.DaggerMovieComponent
import com.example.movieapp.dagger.MovieModule

class MySubApplication : Application() {
    private var cacheServer: HttpProxyCacheServer? = null


    companion object {
        @JvmStatic
        var component: DaggerMovieComponent? = null
        var application: MySubApplication? = null

    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerMovieComponent.builder().movieModule(MovieModule(applicationContext))
            .build() as DaggerMovieComponent?

        application = this

    }

    fun getCacheServer(context: Context?): HttpProxyCacheServer? {
        if (application?.cacheServer == null) {
            application?.cacheServer = application?.buildHttpCacheServer()

        }
        return application?.cacheServer
    }

    private fun buildHttpCacheServer(): HttpProxyCacheServer? {
        return HttpProxyCacheServer.Builder(this)
            .cacheDirectory(cacheDir)
            .build()
    }

}