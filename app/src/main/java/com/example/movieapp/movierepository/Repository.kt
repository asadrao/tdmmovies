package com.example.movieapp.movierepository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.movieapp.mysubapplication.MySubApplication
import com.example.movieapp.services.RetrofitClient
import com.example.movieapp.constantVal.ConstantVal
import com.example.movieapp.interfaces.MovieDataServices
import com.example.movieapp.model.MovieResult
import com.example.movieapp.model.SearchDBResponse
import com.example.movieapp.model.SearchResult
import com.example.movieapp.model.detailmodel.MoviesDetailResult
import com.example.movieapp.services.ObserverResponse
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class Repository @Inject constructor() {

    private val liveDataMoviesList = MutableLiveData<List<MovieResult>>()
    private val movieDetailResult = MutableLiveData<MoviesDetailResult>()
    private val movieSearchResponse = MutableLiveData<List<SearchDBResponse>>()

    var observerResponse: ObserverResponse = MySubApplication.component!!.provideApiResponse()!!


    // fetch Movies List
    fun getAllMoviesList(): MutableLiveData<List<MovieResult>> {
        Log.e("LiveData Repository2", "LiveData ViewModel Repository")
        val requestInterface: MovieDataServices = RetrofitClient.getClient
        requestInterface.getAllMoviesObserv(ConstantVal.API_KEY, ConstantVal.LANGUAGE, 1)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(observerResponse.getAllMoviesListObserver(liveDataMoviesList))
        return liveDataMoviesList
    }

    // fetch Movies Details
    fun getAllMoviesDetail(movieId: Int): MutableLiveData<MoviesDetailResult> {


        val requestInterface2: MovieDataServices = RetrofitClient.getClient
        requestInterface2.getDetails(movieId, ConstantVal.API_KEY)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(observerResponse.getALlMoviesDetailObserver(movieDetailResult))
        return movieDetailResult
    }

    // fetch Movies List
    fun getSearchList(query:String): MutableLiveData<List<SearchDBResponse>> {
        val requestInterface3: MovieDataServices = RetrofitClient.getClient
        requestInterface3.searchMovies(ConstantVal.API_KEY,query)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribeWith(observerResponse.getMoviesQueryResult(movieSearchResponse))
        return movieSearchResponse
    }



}

