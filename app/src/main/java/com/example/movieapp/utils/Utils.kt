package com.example.movieapp.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.facebook.shimmer.ShimmerFrameLayout
import javax.inject.Inject


class Utils @Inject constructor(){
    fun isConnectingToInternet(context: Context?): Boolean {
        val connectivity = context?.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        if (connectivity != null) {
            val info = connectivity.allNetworkInfo
            if (info != null) for (i in info.indices) if (info[i]
                    .state == NetworkInfo.State.CONNECTED
            ) {
                return true
            }
        }
        return false
    }
    fun shimmerProgressViewsUnVisible(shimmer: ShimmerFrameLayout?, progress: ProgressBar?){
        shimmer?.stopShimmer()
        shimmer!!.visibility = View.GONE
        progress?.visibility = View.GONE
    }
    fun toastMessage(context: Context?, message: String?) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
    fun logMessage(messgae:String?, data_value: String?){
        Log.e(messgae,data_value)
    }
}
