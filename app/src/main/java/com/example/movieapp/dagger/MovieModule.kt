package com.example.movieapp.dagger

import android.content.Context
import com.example.movieapp.movierepository.Repository
import com.example.movieapp.services.ObserverResponse
import com.example.movieapp.utils.Utils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class MovieModule constructor(val context:Context){


    @Provides
    @Singleton
    open fun provideObserverResponse(): ObserverResponse? {
        return ObserverResponse(context)
    }
    @Provides
    @Singleton
    open fun provideRepository(): Repository? {
        return Repository()
    }
    @Provides
    @Singleton
    open fun provideUtils(): Utils? {
        return Utils()
    }

}