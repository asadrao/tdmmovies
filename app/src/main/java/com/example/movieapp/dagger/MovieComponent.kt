package com.example.movieapp.dagger

import com.example.movieapp.movierepository.Repository
import com.example.movieapp.services.ObserverResponse
import com.example.movieapp.utils.Utils
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [MovieModule::class])
interface MovieComponent {
      fun provideApiResponse(): ObserverResponse?
      fun provideRepository(): Repository?
      fun  provideUtils(): Utils?
}